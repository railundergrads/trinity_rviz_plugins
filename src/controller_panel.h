#ifndef CONTROLLER_PANEL_H
#define CONTROLLER_PANEL_H

#include <ros/ros.h>
#include <rviz/panel.h>

class QLineEdit;

namespace trinity_rviz_plugins
{
    class ControlWidget;
    class ControllerPanel: public rviz::panel
    {

        Q_OBJECT
        public:
            ControllerPanel( QWidget* parent = 0);

            virtual void load( const rviz::Config& config );
            virtual void save( rviz::Config config) const;

    public Q_SLOTS:
        void setVel( float linear_velocity_, float angular_velocity_ );
    
        void setTopic( const QString& topic);


    protected Q_SLOTS:
        void updateTopic();

    protected:
        QLineEdit* output_topic_editor_;
        
        QString output_topic_;
        
        ros::publisher velocity_publisher_;

        ros::NodeHandle n_;

        float linear_velocity_;
        float angular_velocity_;
        

    };
}

#endif