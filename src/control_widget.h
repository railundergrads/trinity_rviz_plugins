#ifndef CONTROL_WIDGET_H
#define CONTROL_WIDGET_H

#include <QWidget>

namespace trinity_rviz_plugins
{
    Q_OBJECT
    public:
        ControlWidget( QWidget* parent = 0)l;

        virtual void paintEvent( QPaintEvent* event );
        virtual void keyPressEvent( QKeyEvent* event );
        virtual void keyReleaseEvent( QKeyEvent* event );
        virtual void leaveEvent(QEvent* event );

        virtual QSize sizeHint() const { return QSize( 150, 150 ); }

    Q_SIGNALS:
        void outputVelocity( float linear, float angular );

    protected:
        void sendVelocitiesFromKeyboard();

        void stop();

        float linear_velocity_;
        float angular_velocity_;
}

#endif