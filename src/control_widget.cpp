#include <stdio.h>

#include <QPainter>
#include <QKeyEvent>

#include "control_widget.h"

namespace trinity_rviz_plugins
{
    ControlWidget::ControlWidget( QWidget* parent )
        : QWidget( parent )
        , linear_velocity_( 0 )
        , angular_velocity_( 0 )
    {
    }

    void ControlWidget::paintEvent( QPaintEvent* event )
    {
        QColor background;
        QColor keys;
        if( isEnabled() )
        {
            background = Qt::white;
            keys = Qt::black;
        }
        else
        {
            background = Qt::black;
            keys = Qt::black;
        }

        int w = width();
        int h = height();
        int size = (( w > h ) ? h : w) - 1;
        int hpad = ( w - size ) / 2;
        int vpad = ( h - size ) / 2;

        QPainter painter( this );
        painter.setBrush( background );
        painter.setPen( keys );

        painter.drawRect( QRect( hpad, vpad, size, size ));

        painter.drawLine( hpad, height() / 3, hpad + size, height() / 3 );
        painter.drawLine( hpad, height() * 2 / 3, hpad + size, height() * 2/ 3 );
        painter.drawLine( width() * 2 / 3, vpad, width() * 2 / 3, vpad + size );
        painter.drawLine( width() * 2 / 3, vpad, width() * 2 / 3, vpad + size );

    }

    void ControlWidget::keyPressEvent( QKeyEvent* event )
    {

    }

    void ControlWidget::keyReleaseEvent( QKeyEvent* event )
    {

    }

    void ControlWidget::leaveEvent( QEvent* event)
    {
        stop();
    }
}