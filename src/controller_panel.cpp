#include <stdio.h>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTimer>

#include <geometry_msgs/Twist.h>

#include "drive_widget.h"
#include "controller_panel.h"


namespace trinity_rviz_plugins
{
    ControllerPanel::ControllerPanel( QWidget* parent )
     : rviz::Panel( parent )
     , linear_velocity_( 0 )
     , angular_velocity_( 0 )
    {
        QHBoxLayout* topic_layout = new QHBoxLayout;
        topic_layout->addWidget( new QLabel( "Output Topic:" ));
        output_topic_editor_ = new QLineEdit;
        topic_layout->addWidget( output_topic_editor_ );

        control_widget_ = new ControlWidget;
    }
}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(trinity_rviz_plugins::ControllerPanel,rviz::Panel )